import pygame
from pygame.locals import *

TITLE = 'Sorcery'
WIDTH, HEIGHT = 800, 600
SIZE = WIDTH, HEIGHT

class Attack:
    def __init__(self, damage, type):
        self.damage = damage
        self.type = type

def make_enemy_type(max_health, attacks, idle_image, hover_image, weak_to = [], immune_to = []):
    class Enemy(pygame.sprite.Sprite):
        def __init__(self):
            pygame.sprite.Sprite.__init__(self)
            self.health = max_health
            self.attacks = attacks
            self.image = idle_image
            self.idle_image = idle_image
            self.hover_image = hover_image
            self.rect = idle_image.get_rect()
            self.weak_to = weak_to
            self.immune_to = immune_to

        def pick_attack(self):
            return random.choice(self.attacks)

        def suffer_attack(self, attack):
            raw_damage = attack.damage
            if attack.type in weak_to:
                damage = raw_damage * 2
            elif attack.type in immune_to:
                damage = raw_damage // 2
            else:
                damage = raw_damage
            self.health = max(0, self.health - damage)

        def is_dead(self):
            return self.health == 0
    return Enemy

Kobold = make_enemy_type(
    max_health = 10,
    attacks = [
        Attack(2, 'pierce'),
    ],
    idle_image = pygame.image.load('kobold1.png'),
    hover_image = pygame.image.load('kobold_hover1.png'),
    weak_to = ['blunt']
)

class BattleScreen:
    def __init__(self, enemies):
        self.enemies = enemies
        column_width = WIDTH / len(enemies)
        for index, enemy in enumerate(enemies):
            enemy.rect.x = (column_width * index) + (column_width // 2) - (enemy.rect.width // 2)
        self.enemy_sprites = pygame.sprite.RenderPlain(self.enemies)
        self.stat_sprites = pygame.sprite.RenderPlain()

    def handle_event(self, event):
        if event.type == MOUSEMOTION:
            for enemy in self.enemies:
                if enemy.image == enemy.hover_image:
                    enemy.image = enemy.idle_image
            for enemy in self.enemies:
                if enemy.rect.collidepoint(event.pos):
                    enemy.image = enemy.hover_image
                    

    def update(self, dt):
        pass

    def draw(self, screen, character):
        self.enemy_sprites.draw(screen)
        self.stat_sprites.draw(screen)

first_encounter = BattleScreen(
    enemies = [
        Kobold(), Kobold()
    ]
)

class Cursor(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('cursor1.png')
        self.rect = self.image.get_rect()

class Character:
    def __init__(self):
        self.max_health = 10
        self.health = self.max_health
        self.max_stamina = 10
        self.stamina = 10
    
def run_game():
    pygame.init()
    screen = pygame.display.set_mode(SIZE)
    pygame.display.set_caption(TITLE)
    pygame.mouse.set_visible(0)

    # Create The Backgound
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((250, 250, 250))

    # Display The Background
    screen.blit(background, (0, 0))
    pygame.display.flip()

    # Prepare Game Objects
    clock = pygame.time.Clock()

    # Main Loop
    cursor = Cursor()
    top_sprites = pygame.sprite.RenderPlain([cursor])
    game_state = first_encounter
    while True:
        clock.tick(60)
        # Handle Input Events
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                pygame.quit()
            elif event.type == MOUSEMOTION:
                cursor.rect.x = event.pos[0]
                cursor.rect.y = event.pos[1]
            game_state.handle_event(event)
        game_state.update(0)
        # Draw Everything
        screen.blit(background, (0, 0))
        game_state.draw(screen, None and character)
        top_sprites.draw(screen)
        pygame.display.flip()

    pygame.quit()

if __name__ == '__main__':
    run_game()
